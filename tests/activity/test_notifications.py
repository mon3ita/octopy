import unittest
from octopy import OctopyClient


class TestNotification(unittest.TestCase):
    def setUp(self):
        with open("tests/token.txt", "r") as file:
            token = file.readlines()[0].strip()

        self.client = OctopyClient(token=token)

    def test_all(self):
        notifications = self.client.activity.notifications.all()
        self.assertTrue(len(notifications) == 0)

    def test_repository(self):
        notifications = self.client.activity.notifications.repository(
            "monzita", "octopy"
        )
        self.assertTrue(len(notifications) == 0)
