import unittest
from octopy import OctopyClient
from octopy.error import OctopyException


class TestStarring(unittest.TestCase):
    def setUp(self):
        with open("tests/token.txt", "r") as file:
            token = file.readlines()[0].strip()

        self.client = OctopyClient(token=token)

    def test_all(self):
        users = self.client.activity.starring.all("octocat", "Spoon-Knife")
        self.assertTrue(len(users) > 0)

    def test_starred(self):
        repos = self.client.activity.starring.starred(username="octocat")
        self.assertTrue(len(repos) > 0)

    def test_am_i_starring(self):
        self.assertRaises(
            OctopyException,
            self.client.activity.starring.am_i_starring,
            "octocat",
            "Spoon-Knife",
        )

    def test_star(self):
        response = self.client.activity.starring.star("monzita", "octopy")
        self.assertTrue(response == 204)

    def test_unstar(self):
        response = self.client.activity.starring.unstar("monzita", "octopy")
        self.assertTrue(response == 204)
