import unittest

from octopy import OctopyClient


class TestEvent(unittest.TestCase):
    def setUp(self):
        with open("tests/token.txt", "r") as file:
            token = file.readlines()[0].strip()

        self.client = OctopyClient(token=token)

    def test_all_events(self):
        events = self.client.activity.events.all()

        self.assertTrue(len(events) > 0 and "repo" in events[0].__dict__)

    def test_all_events_next_page(self):
        events = self.client.activity.events.all(page=2)
        self.assertTrue(len(events) > 0 and "repo" in events[0].__dict__)

    def test_repository_events(self):
        events = self.client.activity.events.repository("octocat", "Spoon-Knife")
        self.assertTrue(len(events) > 0)

    def test_repository_issues(self):
        events = self.client.activity.events.issue("octocat", "Spoon-Knife")
        self.assertTrue(len(events) > 0)

    def test_events_network(self):
        events = self.client.activity.events.network("octocat", "octocat.github.io")
        self.assertTrue(len(events) > 0)

    def test_org_events(self):
        events = self.client.activity.events.organization("github")
        self.assertTrue(len(events) > 0)

    def test_user_events(self):
        events = self.client.activity.events.received("octocat")
        self.assertTrue(len(events) > 0)

    def test_user_performed_events(self):
        events = self.client.activity.events.performed("github")
        self.assertTrue(len(events) > 0)
