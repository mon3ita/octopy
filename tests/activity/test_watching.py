import unittest
from octopy import OctopyClient


class TestWatching(unittest.TestCase):
    def setUp(self):
        with open("tests/token.txt", "r") as file:
            token = file.readlines()[0].strip()

        self.client = OctopyClient(token=token)

    def test_all(self):
        watched = self.client.activity.watching.all("octocat", "Spoon-Knife")
        self.assertTrue(len(watched) > 0)

    def test_repo(self):
        watched = self.client.activity.watching.repository(username="octocat")
        self.assertTrue(len(watched) > 0)

    def test_subscribe(self):
        response = self.client.activity.watching.subscribe("monzita", "sceleton")
        self.assertTrue(response.subscribed)

    def test_subscribed(self):
        response = self.client.activity.watching.subscribed("monzita", "sceleton")
        self.assertTrue(response.subscribed)

    def test_unsubscribe(self):
        response = self.client.activity.watching.unsubscribe("monzita", "sceleton")
        self.assertTrue(response == 204)
