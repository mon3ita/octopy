import unittest
from octopy import OctopyClient


class TestFeed(unittest.TestCase):
    def setUp(self):
        with open("tests/token.txt", "r") as file:
            token = file.readlines()[0].strip()

        self.client = OctopyClient(token=token)

    def test_feeds(self):
        feeds = self.client.activity.feeds.all()
        self.assertTrue("current_user_public_url" in feeds.__dict__)
