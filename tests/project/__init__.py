import unittest
from octopy import OctopyClient


class TestProject(unittest.TestCase):
    def setUp(self):
        with open("tests/token.txt", "r") as file:
            token = file.readlines()[0].strip()

        self.client = OctopyClient(token=token)

        self.project_name = "fake"
        self.username = "monzita"
        self.projects = self.client.projects.all(self.username, self.project_name)

    def test_all(self):
        self.assertTrue(len(self.projects) > 0)

    def test_create(self):
        project = self.client.projects.create(
            self.username, self.project_name, name="test", description="test project"
        )
        self.assertTrue(project.name == "test")

    def test_create_user_projects(self):
        project = self.client.projects.create(
            self.username,
            self.project_name,
            project_type="user",
            name="test",
            body="test project",
        )
        self.assertTrue(project.name == "test")

    def test_update(self):
        try:
            project = self.client.projects.update(
                self.projects[0].id, name="updated", description="new description"
            )
            self.assertTrue(project.name == "updated")
        except:
            pass

    def test_user(self):
        projects = self.client.projects.user(self.username)
        self.assertTrue(len(projects) > 0)

    def test_create_column(self):
        try:
            column = self.client.projects.columns.create(
                self.projects[0].id, name="test"
            )
            self.assertTrue(column.name == "test")
        except:
            pass

    def test_move_column(self):
        columns = self.client.projects.columns.all(self.projects[0].id)
        if columns:
            response = self.client.projects.columns.move(columns[0].id, position="last")
            self.assertTrue(response.status_code == 201)

    def test_remove_column(self):
        columns = self.client.projects.columns.all(self.projects[0].id)
        response = []
        for column in columns:
            response.append(self.client.projects.columns.remove(column.id))

        self.assertTrue(
            len(list(filter(lambda r: r.status_code == 204, response))) == len(response)
        )

    def test_remove_projects(self):
        response = []
        for project in self.projects:
            response.append(self.client.projects.remove(project.id))

        self.assertTrue(
            len(list(filter(lambda r: r.status_code == 204, response))) == len(response)
        )
