import unittest
from octopy import OctopyClient
from octopy.error import OctopyException


class TestPullRequest(unittest.TestCase):
    def setUp(self):
        with open("tests/token.txt", "r") as file:
            token = file.readlines()[0].strip()

        self.client = OctopyClient(token=token)

        self.project_name = "newpyfile"
        self.username = "monzita"
        self.pulls = self.client.pull_requests.all(self.username, self.project_name)

    def test_get_all(self):
        self.assertTrue(len(self.pulls) > 0)

    def test_get_single(self):
        pull = self.client.pull_requests.get(
            self.username, self.project_name, self.pulls[0].number
        )
        self.assertTrue(pull.user.login == self.username)

    def test_get_commits(self):
        commits = self.client.pull_requests.commits(
            self.username, self.project_name, self.pulls[0].number
        )
        self.assertTrue(len(commits) > 0)

    def test_merged(self):
        self.assertRaises(
            OctopyException,
            self.client.pull_requests.merged,
            self.username,
            self.project_name,
            self.pulls[0].number,
        )
