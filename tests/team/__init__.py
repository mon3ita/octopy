import unittest

from octopy import OctopyClient


class TestTeam(unittest.TestCase):
    def setUp(self):
        with open("tests/token.txt", "r") as file:
            token = file.readlines()[0].strip()

        self.client = OctopyClient(token=token)
        self.username = "monzita"
        self.org = "DAlgs"
        self.teams = self.client.teams.all(self.org)

    def test_create(self):
        team = self.client.teams.create(
            self.org, name="Test", description="This is just a text"
        )
        self.assertTrue(team.organization.login == self.org)

    def test_all(self):
        self.assertTrue(len(self.teams) > 0)

    def test_user_teams(self):
        teams = self.client.teams.user()
        self.assertTrue(len(teams) > 0)

    def test_update(self):
        try:
            team = self.teams[0]
            self.assertTrue(
                self.client.teams.update(
                    team.id, name="New Test Name"
                ).organization.login
                == self.org
            )
        except:
            pass

    def test_remove_teams(self):
        response = []
        for team in self.teams:
            response.append(self.client.teams.remove(team.id).status_code)
        self.assertTrue(
            len(list(filter(lambda r: r == 204, response))) == len(response)
        )

    def test_create_discussion(self):
        try:
            discussion = self.client.teams.discussions.create(
                self.teams[0].id, title="Test discussion", body="Test body"
            )
            self.assertTrue(discussion.author.login == self.username)
        except:
            pass

    def test_create_discussion_comment(self):
        try:
            discussions = self.client.teams.discussions.all(self.teams[0].id)
            comment = self.client.teams.discussion_comments.create(
                self.teams[0].id, discussions[0].number, body="Test comment"
            )
            self.assertTrue(comment.author.login == self.username)
        except:
            pass

    def test_update_discussion(self):
        try:
            discussions = self.client.teams.discussions.all(self.teams[0].id)
            updated = self.client.teams.discussions.update(
                self.teams[0].id, discussions[0].number, title="Updated"
            )
            self.assertTrue(updated.author.login == self.username)
        except:
            pass

    def test_update_comment(self):
        try:
            discussions = self.client.teams.discussions.all(self.teams[0].id)
            comments = self.client.teams.discussion_comments.all(
                self.teams[0].id, discussions[0].number
            )
            updated = self.client.teams.discussion_comments.update(
                self.teams[0].id,
                discussions[0].number,
                comments[0].number,
                body="Updated",
            )
            self.assertTrue(updated.author.login == self.username)
        except:
            pass
