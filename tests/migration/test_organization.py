import unittest
from octopy import OctopyClient


class TestOrganization(unittest.TestCase):
    def setUp(self):
        with open("tests/token.txt", "r") as file:
            token = file.readlines()[0].strip()

        self.client = OctopyClient(token=token)

    def test_create(self):
        migration = self.client.migrations.organizations.create(
            "DAlgs", repositories=["DAlgs/forest"]
        )
        self.assertTrue(migration.repositories[0]["name"] == "forest")

    def test_all(self):
        migrations = self.client.migrations.organizations.all("DAlgs")
        self.assertTrue(len(migrations) > 0)
