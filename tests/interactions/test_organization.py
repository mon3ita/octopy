import unittest
from octopy import OctopyClient


class TestOrganization(unittest.TestCase):
    def setUp(self):
        with open("tests/token.txt", "r") as file:
            token = file.readlines()[0].strip()

        self.client = OctopyClient(token=token)

    def test_restrictions(self):
        response = self.client.interactions.organization.restrictions("dalgs")
        self.assertTrue(response)

    def test_update(self):
        response = self.client.interactions.organization.update(
            "dalgs", limit="collaborators_only"
        )
        self.assertTrue(response)

    def test_remove(self):
        response = self.client.interactions.organization.remove("dalgs")
        self.assertTrue(response.status_code == 204)
