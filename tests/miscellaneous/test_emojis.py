import unittest
from octopy import OctopyClient


class TestEmojis(unittest.TestCase):
    def setUp(self):
        with open("tests/token.txt", "r") as file:
            token = file.readlines()[0].strip()

        self.client = OctopyClient(token=token)

    def test_all(self):
        emojis = self.client.miscellaneous.emojis.all()
        self.assertTrue(len(emojis.keys()) > 0)
