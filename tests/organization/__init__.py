import unittest
from octopy import OctopyClient


class TestOrganization(unittest.TestCase):
    def setUp(self):
        with open("tests/token.txt", "r") as file:
            token = file.readlines()[0].strip()

        self.client = OctopyClient(token=token)

    def test_mine(self):
        orgs = self.client.organizations.mine()
        self.assertTrue(orgs[0].login == "DAlgs")

    def test_all(self):
        orgs = self.client.organizations.all()
        self.assertTrue(len(orgs) > 0)

    def test_user(self):
        orgs = self.client.organizations.user("monzita")
        self.assertTrue(orgs[0].login == "DAlgs")

    def test_insllations(self):
        instl = self.client.organizations.installations("DAlgs")
        self.assertTrue(len(instl) == 0)

    def test_get(self):
        org = self.client.organizations.get("DAlgs")
        self.assertTrue(org.login == "DAlgs")

    def test_all_blocked_users(self):
        users = self.client.organizations.blocking_users.all("DAlgs")
        self.assertTrue(len(users) == 0)

    def test_get_members(self):
        users = self.client.organizations.members.all("DAlgs")
        self.assertTrue(len(users) > 0)
