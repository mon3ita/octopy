import unittest
from octopy import OctopyClient


class TestGistComments(unittest.TestCase):
    def setUp(self):
        with open("tests/token.txt", "r") as file:
            token = file.readlines()[0].strip()

        self.client = OctopyClient(token=token)
        self.gists = self.client.gists.mine()
        try:
            self.comments = self.client.gists.comments.all(self.gists[0].id)
        except:
            self.comments = []

    def test_create_gist(self):
        gist = self.client.gists.create(
            files={"test.txt": {"content": "Just a test gist"}},
            description="This is a test gist",
        )
        self.assertTrue(gist.owner.login == "monzita")

    def test_all(self):
        try:
            self.assertTrue(len(self.comments) > 0)
        except:
            pass

    def test_get(self):
        try:
            comment = self.client.gists.comments.get(
                self.gists[0].id, self.comments[0].id
            )
            self.assertTrue("created_at" in comment.__dict__)
        except:
            pass

    def test_create_comment(self):
        try:
            comment = self.client.gists.comments.comment(
                self.gists[0].id, body="Just a test comment"
            )
            self.assertTrue("created_at" in comment.__dict__)
        except:
            pass

    def test_update_comments(self):
        try:
            comment = self.client.gists.comments.update(
                self.gists[0].id, self.comments[0].id, body="Just updated"
            )
            self.assertTrue("created_at" in comment.__dict__)
        except:
            pass

    def test_remove_comment(self):
        response = []
        for comment in self.comments:
            response.append(
                self.client.gists.comments.remove(self.gists[0].id, comment.id)
            )

        self.assertTrue(
            len(list(filter(lambda r: r.status_code == 204, response))) == len(response)
        )

    def test_remove_gists(self):
        response = []
        for gist in self.gists:
            response.append(self.client.gists.remove(gist.id))

        self.assertTrue(
            len(list(filter(lambda r: r.status_code == 204, response))) == len(response)
        )
