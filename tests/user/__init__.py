import unittest

from octopy import OctopyClient


class TestUser(unittest.TestCase):
    def setUp(self):
        with open("tests/token.txt", "r") as file:
            token = file.readlines()[0].strip()

        self.client = OctopyClient(token=token)

        self.username = "monzita"
        self.target = "octopy-test"
        self.users = self.client.users.all()

    def test_get(self):
        user = self.client.users.get(self.username)
        self.assertTrue(user.login == self.username)

    def test_get_me(self):
        me = self.client.users.me()
        self.assertTrue(me.login == self.username)

    def test_update_user(self):
        updated = self.client.users.update(blog="just_a_test.com")
        self.assertTrue(updated.login == self.username)

    def test_user_block(self):
        response = []

        try:
            response.append(
                self.client.users.blocking_users.block(self.target).status_code
            )
        except:
            response.append(404)
        try:
            response.append(
                self.client.users.blocking_users.unblock(self.target).status_code
            )
        except:
            response.append(404)

        self.assertTrue(
            len(list(filter(lambda r: r == 204, response))) == len(response)
        )

    def test_user_followers(self):
        response = []

        response.append(
            self.client.users.followers.follow(self.target).status_code == 204
        )
        response.append(len(self.client.users.followers.user(self.target)) > 0)
        response.append(
            self.client.users.followers.am_i_following(self.target).status_code == 204
        )
        response.append(len(self.client.users.followers.my_followings()) > 0)
        response.append(
            self.client.users.followers.unfollow(self.target).status_code == 204
        )

        self.assertTrue(
            len(list(filter(lambda r: r == True, response))) == len(response)
        )

    def test_emails(self):
        response = []
        response.append(len(self.client.users.emails.mine()) > 0)
        response.append(len(self.client.users.emails.mine(public=True)) > 0)
        self.assertTrue(
            len(list(filter(lambda r: r == True, response))) == len(response)
        )

    def test_git_ssh_keys(self):
        response = []

        key = None
        with open("tests/key.txt", "r") as f:
            key = f.readlines()[0].strip()

        response.append(
            self.client.users.git_ssh_keys.create(title="test@key", key=key).read_only
        )

        keys = self.client.users.git_ssh_keys.mine()
        response.append(len(keys) > 0)
        response.append(self.client.users.git_ssh_keys.get(keys[0].id).read_only)
        response.append(
            self.client.users.git_ssh_keys.remove(keys[0].id).status_code == 204
        )
        self.assertTrue(
            len(list(filter(lambda r: r == True, response))) == len(response)
        )
