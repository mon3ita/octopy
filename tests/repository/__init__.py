import unittest

from octopy import OctopyClient


class TestRepository(unittest.TestCase):
    def setUp(self):
        with open("tests/token.txt", "r") as file:
            token = file.readlines()[0].strip()

        self.client = OctopyClient(token=token)
        self.username = "monzita"

    def test_create_repo(self):
        try:
            self.client.repositories.get(self.username, "test")
        except:
            try:
                self.client.repositories.get(self.username, "updated")
            except:
                repo = self.client.repositories.create(name="test", private=True)
                self.assertTrue(repo.private)

    def test_update_repo(self):
        try:
            repo = self.client.repositories.update(
                self.username, "test", name="updated"
            )
            self.assertTrue(repo.name == "updated")
        except:
            repo = self.client.repositories.update(
                self.username, "updated", name="updated"
            )
            self.assertTrue(repo.name == "updated")
