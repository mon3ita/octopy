import unittest

from time import time

import jwt

from octopy import OctopyClient


class TestApp(unittest.TestCase):
    def setUp(self):
        with open("tests/token.txt", "r") as file:
            token = file.readlines()[0].strip()

        self.client = OctopyClient(token=token)
        self.username = "monzita"

    def test_get(self):
        app = self.client.apps.get("octopyy")
        self.assertTrue(app.owner.login == self.username)

    def test_installations(self):
        with open("tests/app/private.pem", "r") as f:
            key = f.read().strip()

        with open("tests/app/secret.txt", "r") as f:
            id_ = int(f.readlines()[0].strip())

        time_ = time()
        payload = {"iat": int(time_), "exp": int(time_) + (3 * 60), "iss": id_}

        jwt_ = jwt.encode(payload, key, algorithm="RS256").decode("utf-8")
        self.client.jwt = jwt_
        installations = self.client.apps.all()
        self.assertTrue(len(installations) == 0)
