import unittest
from octopy import OctopyClient


class TestRef(unittest.TestCase):
    def setUp(self):
        with open("tests/token.txt", "r") as file:
            token = file.readlines()[0].strip()

        self.client = OctopyClient(token=token)

    def test_get(self):
        ref = self.client.data.references.get("monzita", "sceleton", "heads/master")

        self.assertTrue(ref)

    def test_matching(self):
        refs = self.client.data.references.matching(
            "monzita", "sceleton", "heads/master"
        )
        self.assertTrue(len(refs) > 0)

    def test_create(self):
        test = self.client.data.references.create(
            "monzita",
            "sceleton",
            ref="refs/heads/v0.0.7",
            sha="9a8347e5c18244f1bd28fe7acc1ae13b94e42d79",
        )
        self.assertTrue(test)

    def test_update(self):
        ref = self.client.data.references.update(
            "monzita",
            "sceleton",
            "heads/v0.0.7",
            sha="9a8347e5c18244f1bd28fe7acc1ae13b94e42d79",
        )

        self.assertTrue(ref)

    def test_delete(self):
        response = self.client.data.references.remove(
            "monzita", "sceleton", "heads/v0.0.7"
        )

        self.assertTrue(response == 204)
