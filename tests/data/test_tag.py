import unittest
from octopy import OctopyClient


class TestTag(unittest.TestCase):
    def setUp(self):
        with open("tests/token.txt", "r") as file:
            token = file.readlines()[0].strip()

        self.client = OctopyClient(token=token)

    def test_create(self):
        commit = self.client.data.tags.create(
            "monzita",
            "sceleton",
            tag="v0.0.6",
            message="Add a tag version",
            object="9a8347e5c18244f1bd28fe7acc1ae13b94e42d79",
            type="commit",
        )

        self.assertTrue(commit.tagger.name == "monzita")

    def test_get(self):
        commit = self.client.data.tags.get(
            "monzita", "sceleton", "4dbb295a6153c418161db97281e6997641bd8432"
        )
        self.assertTrue(commit.tagger.name == "monzita")
