import unittest
from octopy import OctopyClient


class TestCommit(unittest.TestCase):
    def setUp(self):
        with open("tests/token.txt", "r") as file:
            token = file.readlines()[0].strip()

        self.client = OctopyClient(token=token)

    def test_get(self):
        commit = self.client.data.commits.get(
            "monzita", "newpyfile", "a29063c1f642a088d6c8114bb2f5f7e0a82c05c3"
        )
        self.assertTrue(commit.author.name == "monzita")
