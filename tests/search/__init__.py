import unittest
from octopy import OctopyClient


class TestSearch(unittest.TestCase):
    def setUp(self):
        with open("tests/token.txt", "r") as file:
            token = file.readlines()[0].strip()

        self.client = OctopyClient(token=token)

    def test_search_repos(self):
        repos = self.client.search.repositories(q="python")
        self.assertTrue(len(repos) > 0)

    def test_search_commits(self):
        commits = self.client.search.commits(q="test")
        self.assertTrue(len(commits) > 0)

    def test_search_users(self):
        users = self.client.search.users(q="octo")
        self.assertTrue(len(users) > 0)
