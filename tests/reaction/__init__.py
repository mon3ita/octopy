import unittest
from octopy import OctopyClient


class TestReactions(unittest.TestCase):
    def setUp(self):
        with open("tests/token.txt", "r") as file:
            token = file.readlines()[0].strip()

        self.client = OctopyClient(token=token)
        self.username = "monzita"
        self.project = "fake"

    def test_get_reactions(self):
        comments = self.client.repositories.comments.all(self.username, self.project)
        reactions = self.client.reactions.all(
            owner=self.username, repo=self.project, comment_id=comments[0].id
        )
        self.assertTrue(len(reactions) > 0)

    def test_create_reaction(self):
        comments = self.client.repositories.comments.all(self.username, self.project)
        reaction = self.client.reactions.create(
            owner=self.username,
            repo=self.project,
            comment_id=comments[0].id,
            content="heart",
        )
        self.assertTrue(reaction.user.login == self.username)

    def test_remove_reaction(self):
        comments = self.client.repositories.comments.all(self.username, self.project)
        reactions = self.client.reactions.all(
            owner=self.username, repo=self.project, comment_id=comments[0].id
        )
        self.assertTrue(
            self.client.reactions.remove(reactions[0].id).status_code == 204
        )
