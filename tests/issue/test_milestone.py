import unittest
from octopy import OctopyClient

from nonsensepy import NonsensePyGen


class TestMilestone(unittest.TestCase):
    def setUp(self):
        with open("tests/token.txt", "r") as file:
            token = file.readlines()[0].strip()

        self.client = OctopyClient(token=token)
        self.milestones = self.client.issues.milestones.all("monzita", "fake")
        self.random = NonsensePyGen()

    def test_create(self):
        milestone = self.client.issues.milestones.create(
            "monzita",
            "fake",
            title=self.random.strrandom(min=5, max=10),
            description="Test milestone",
        )
        self.assertTrue(milestone.creator.login == "monzita")

    def test_all(self):
        self.assertTrue(len(self.milestones) > 0)

    def test_get(self):
        try:
            milestone = self.client.issues.milestones.get(
                "monzita", "fake", self.milestones[0].number
            )
            self.assertTrue(milestone.creator.login == "monzita")
        except:
            pass

    def test_update(self):
        try:
            milestone = self.client.issues.milestones.update(
                "monzita", "fake", self.milestones[0].number, title="test_updated"
            )
            self.assertTrue(milestone.title == "test_updated")
        except:
            pass

    def test_remove(self):
        response = []
        for milestone in self.milestones:
            response.append(
                self.client.issues.milestones.remove(
                    "monzita", "fake", milestone.number
                )
            )

        self.assertTrue(
            len(list(filter(lambda r: r.status_code == 204, response))) == len(response)
        )
