import unittest
from octopy import OctopyClient


class TestLabel(unittest.TestCase):
    def setUp(self):
        with open("tests/token.txt", "r") as file:
            token = file.readlines()[0].strip()

        self.client = OctopyClient(token=token)

    def test_create(self):
        label = self.client.issues.labels.create(
            "monzita",
            "fake",
            name="test_label",
            color="FF0099",
            description="Test label",
        )
        self.assertTrue(label.name == "test_label")

    def test_add_to_issue(self):
        labels = self.client.issues.labels.create_issue_label(
            "monzita", "fake", 1, labels=["l1", "l2", "l3"]
        )
        self.assertTrue(len(labels) == 3)

    def test_get_label(self):
        label = self.client.issues.labels.get("monzita", "fake", "test_label")
        self.assertTrue(label.color == "FF0099")

    def test_remove_label(self):
        response = self.client.issues.labels.remove("monzita", "fake", "test_label")
        self.assertTrue(response.status_code == 204)
