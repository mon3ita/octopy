import unittest
from octopy import OctopyClient


class TestIssue(unittest.TestCase):
    def setUp(self):
        with open("tests/token.txt", "r") as file:
            token = file.readlines()[0].strip()

        self.client = OctopyClient(token=token)

    def test_create(self):
        issue = self.client.issues.create("monzita", "fake", title="Test issue")
        self.assertTrue(issue.user.login == "monzita")

    def test_repository(self):
        issues = self.client.issues.repository("monzita", "fake", reactions=True)
        self.assertTrue(len(issues) > 0)

    def test_get(self):
        issue = self.client.issues.get("monzita", "fake", 1)
        self.assertTrue(issue.user.login == "monzita")

    def test_lock(self):
        self.assertTrue(self.client.issues.lock("monzita", "fake", 1))
