import unittest
from octopy import OctopyClient


class TestComment(unittest.TestCase):
    def setUp(self):
        with open("tests/token.txt", "r") as file:
            token = file.readlines()[0].strip()

        self.client = OctopyClient(token=token)
        self.comments = self.client.issues.comments.all("monzita", "fake", 1)

    def test_create_comment(self):
        comment = self.client.issues.comments.create(
            "monzita", "fake", 1, body="Test comment"
        )
        self.assertTrue(comment.user.login == "monzita")

    def test_get_comment(self):

        try:
            comment = self.comments[0]
            self.assertTrue(comment.user.login == "monzita")
        except:
            pass

    def test_update_comment(self):
        try:
            comment = self.client.issues.comments.update(
                "monzita", "fake", self.comments[0].id, body="Updated"
            )
            self.assertTrue(comment.body == "Updated")
        except:
            pass

    def test_all_comments(self):
        self.assertTrue(len(self.comments) > 0)

    def test_remove_comment(self):
        response = []
        for comment in self.comments:
            response.append(
                self.client.issues.comments.remove("monzita", "fake", comment.id)
            )

        self.assertTrue(
            len(list(filter(lambda r: r == 204, response))) == len(response)
        )
