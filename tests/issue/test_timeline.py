import unittest
from octopy import OctopyClient


class TestTimeline(unittest.TestCase):
    def setUp(self):
        with open("tests/token.txt", "r") as file:
            token = file.readlines()[0].strip()

        self.client = OctopyClient(token=token)

    def test_all(self):
        events = self.client.issues.timeline.all("monzita", "fake", 1)
        self.assertTrue(len(events) > 0)
