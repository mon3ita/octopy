import unittest
from octopy import OctopyClient


class TestAssignee(unittest.TestCase):
    def setUp(self):
        with open("tests/token.txt", "r") as file:
            token = file.readlines()[0].strip()

        self.client = OctopyClient(token=token)

    def test_all(self):
        assignees = self.client.issues.assignees.all("monzita", "octopy")
        self.assertTrue(len(assignees) > 0)

    def test_check(self):
        response = self.client.issues.assignees.check("monzita", "octopy", "monzita")
        self.assertTrue(response.status_code == 204)

    def test_create_assignee(self):
        response = self.client.issues.assignees.create(
            "monzita", "fake", 1, assignees=["monzita"]
        )
        self.assertTrue(response.user.login == "monzita")

    def test_delete_assignee(self):
        response = self.client.issues.assignees.remove(
            "monzita", "fake", 1, assignees=["monzita"]
        )
        self.assertTrue(response.user.login == "monzita")
