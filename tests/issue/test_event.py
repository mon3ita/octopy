import unittest
from octopy import OctopyClient


class TestEvent(unittest.TestCase):
    def setUp(self):
        with open("tests/token.txt", "r") as file:
            token = file.readlines()[0].strip()

        self.client = OctopyClient(token=token)
        self.events = self.client.issues.events.all("monzita", "fake", 1)

    def test_all(self):
        self.assertTrue(len(self.events) > 0)

    def test_all_repo(self):
        events = self.client.issues.events.repository("monzita", "fake")
        self.assertTrue(len(events) > 0)

    def test_get(self):
        try:
            event = self.client.issues.events.get("monzita", "fake", self.events[0].id)
            self.assertTrue(event.actor.login == "monzita")
        except:
            pass
