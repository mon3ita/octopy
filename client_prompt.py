from octopy.oauth2 import OctopyAuth

client_id = input("Client id: ").strip()
client_secret = input("Client secret: ").strip()
scope = input("Scopes: ").strip().split(" ")
redirect_uri = input("Redirect URL: ").strip()

oauth = OctopyAuth(
    client_id=client_id,
    client_secret=client_secret,
    scope=scope,
    redirect_uri=redirect_uri,
)

url = oauth.authorize()

print("Visit: {}".format(url))

response_url = input("Copy url: ")

token = oauth.authentication_token(url=response_url)

print("Your token: {}".format(token))
